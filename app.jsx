import React from 'react';

import { AppLayer } from '../../turtleos-am/am';

import { WebView } from '../../turtleos-nm/webview';
import {PERMISSION} from '../../../lib/turtleos-permissions/permissions';

import icon from './icon.png';

function BrowserHeaderIcon({onClick=()=>{}, icon}) {
    const [hover, setHover] = React.useState(false);
    return (
        <div style={{
            borderRadius:'50px',
            background:(hover)?'#a89984':'transparent',
            width:'2rem',
            height:'2rem',
            transition:'background .3s',
            display:'flex',
            alignItems:'center',
            justifyContent:'center',
            margin:'0px 0.1rem',
            padding:'0.2rem'
        }} onClick={onClick} onMouseEnter={()=>setHover(true)} onMouseLeave={()=>setHover(false)}>
          {icon}
        </div>
    );
}

function App({appdk, intent, bridge}) {
  const [ index, setIndex ] = React.useState('https://duckduckgo.com');
  const [url, setURL] = React.useState('https://duckduckgo.com');

    React.useEffect(()=>{
        return appdk.AM.IntentResolver({
            intent: intent,
            bridge: bridge
        },(intentd)=>{
            if(!!intentd.url) {
                setIndex(intentd.url);
                setURL(intentd.url);
            }
        });
    },[]);

    function renderWebView(url) {
        return (<WebView config={{
            style:{
                width:'100%',
                height:'100%',
                background:'white'
            },

        }} onChange={(e)=>setURL(e)} url={url}/>);
    }

  const update = (url) => {
    let resolved = appdk.Network.resolve(url);
    setIndex(resolved);
    setURL(resolved);
  }

    return (
        <div style={{
            position:'absolute',
            top:0,
            left:0,
            width:'100%',
            height:'100%',
            display:'flex',
            flexDirection:'column',
            justifyContent:'center',
            alignItems:'center'
        }}>
          {/*Viewport*/}
          <div style={{
              position:'absolute',
              height:'calc(100% - 3rem)',
              width:'100%',
              left:0,
              bottom:0,
          }}>
            {renderWebView(index)}
          </div>

          {/*URl Bar*/}
          <div style={{
              position:'absolute',
              top:0,
              left:0,
              width:'100%',
              height:'3rem',
              background:'#3c3836',
              boxShadow:'0px 0px 10px -5px black',
              display:'flex',
              flexDirection:'row',
              alignItems:'center',
              justifyContent:'space-evenly',
              color:'#ebdbb2',
              size:48
          }}>
            <div style={{
                display:'flex',
                flexDirection:'row'
            }}>
            </div>
            <input onChange={(e)=>{setURL(e.target.value);}} style={{
                background:'#504945',
                padding:'0.2rem',
                width:'70%',
                color:'#ebdbb2',
                border:'none',
                borderRadius:'25px',
                outline:'none'
            }} name="url" type="text" value={url}
               onKeyDown={(e)=>{
                 if (e.key === 'Enter') {
                   update(url);
                 }}}
            />
            <BrowserHeaderIcon onClick={()=>{
              update(url);
            }} icon={<appdk.UI.ICONS.Feather.Search/>}/>
          </div>
        </div>
    );
}

const LAYER = new AppLayer('Lattus Browser', 'icu.ccw.turtleos.lattus',{
    render: ( {intent={},bridge, appdk})=>{return (<App appdk={appdk} intent={intent} bridge={bridge}/>);},
    icon: icon,
    requests: {
        [PERMISSION.NETWORK]:true,
    },
    forceGrant: {
        [PERMISSION.NETWORK]:true
    }
});

export {
    LAYER
}
